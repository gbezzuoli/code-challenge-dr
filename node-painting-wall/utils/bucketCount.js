const bucketCount = async (total) => {
  const totalLitros = total / 5;

  const bucketsCounter = {
    large18: 0,
    medium3six: 0,
    small2five: 0,
    mini0five: 0,
    total: total,
    totalLitros: totalLitros,
    remainder: 0,
    totalBuckets: '',
  };

  while (total > 0) {
    if (total >= 90) {
      total -= 90;
      bucketsCounter.large18 += 1;
    } else if (total >= 18 ) {
      total -= 18;
      bucketsCounter.medium3six += 1;
    } else if (total >= 12.5) {
      total -= 12.5;
      bucketsCounter.small2five += 1;
    } else {
      total -= 2.5;
      const a = Math.abs(total);
      bucketsCounter.remainder = a;
      bucketsCounter.mini0five += 1;
      const totalBuckets = bucketsCounter.large18 + bucketsCounter.medium3six + bucketsCounter.small2five + bucketsCounter.mini0five;
      bucketsCounter.totalBuckets = totalBuckets;
    }
  }

  
  console.log('bucketsCounter', bucketsCounter);
  return bucketsCounter;
};

module.exports = {
  bucketCount,
};

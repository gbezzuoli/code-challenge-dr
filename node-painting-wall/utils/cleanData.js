const cleanData = async (walls, req, res, next) => {
  console.log('walls cleandata', walls);
  const wall1 = []; // 1
  const wall2 = []; // 2
  const wall3 = []; // 3
  const wall4 = []; // 4
  const wallsCount = [wall1, wall2, wall3, wall4]; // array que reune todas as paredes

  const ArrayTojson = (array) => { // função que transforma array em json
    const json = {};
    for (let i = 0; i < array.length; i++) {
      json[`wall${i + 1}`] = {
        height: array[i][0],
        width: array[i][1],
        doors: array[i][2],
        windows: array[i][3],
      };
    }
    return json;
  };

  wall1.push(walls.wall1height);
  wall1.push(walls.wall1width);
  wall1.push(walls.wall1doors);
  wall1.push(walls.wall1window);
  // bloco que insere os dados do body na array wall1

  wall2.push(walls.wall2height);
  wall2.push(walls.wall2width);
  wall2.push(walls.wall2doors);
  wall2.push(walls.wall2window);
  // bloco que insere os dados do body na array wall2

  wall3.push(walls.wall3height);
  wall3.push(walls.wall3width);
  wall3.push(walls.wall3doors);
  wall3.push(walls.wall1window);
  // bloco que insere os dados do body na array wall3

  wall4.push(walls.wall4height);
  wall4.push(walls.wall4width);
  wall4.push(walls.wall4doors);
  wall4.push(walls.wall4window);
  // bloco que insere os dados do body na array wall4

  const wallsJson = ArrayTojson(wallsCount); // transforma array em json
  console.log('wallsJson', wallsJson);
  return wallsJson; // retorna json
};

module.exports = {
  cleanData,
};

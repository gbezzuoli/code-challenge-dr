const paintWallModel = require('../models/paintWallModel');

const wallCalculator = async (wallsJson) => {
  const wall1doorArea = wallsJson.wall1.doors * (0.8 * 1.9);
  const wall1windowArea = wallsJson.wall1.windows * (2 * 1.2);
  const wall1Area = wallsJson.wall1.height
   * wallsJson.wall1.width
   - (wall1doorArea - wall1windowArea);
   // calcula a area da parede 1

  const wall2doorArea = wallsJson.wall2.doors * (0.8 * 1.9);
  const wall2windowArea = wallsJson.wall2.windows * (2 * 1.2);
  const wall2Area = wallsJson.wall2.height
   * wallsJson.wall2.width
   - (wall2doorArea - wall2windowArea);
    // calcula a area da parede 2

  const wall3doorArea = wallsJson.wall3.doors * (0.8 * 1.9);
  const wall3windowArea = wallsJson.wall3.windows * (2 * 1.2);
  const wall3Area = wallsJson.wall3.height
   * wallsJson.wall3.width
   - (wall3doorArea - wall3windowArea);
    // calcula a area da parede 3

  const wall4doorArea = wallsJson.wall4.doors * (0.8 * 1.9);
  const wall4windowArea = wallsJson.wall4.windows * (2 * 1.2);
  const wall4Area = wallsJson.wall4.height
   * wallsJson.wall4.width
   - (wall4doorArea - wall4windowArea);
    // calcula a area da parede 4

  const totalArea = wallsJson.wall1.height
   * wallsJson.wall1.width
   + wallsJson.wall2.height
    * wallsJson.wall2.width
    + wallsJson.wall3.height
     * wallsJson.wall3.width
     + wallsJson.wall4.height
      * wallsJson.wall4.width;
        // calcula a area total do quarto/sala

  const door = +wallsJson.wall1.doors
   + +wallsJson.wall2.doors
    + +wallsJson.wall3.doors
     + +wallsJson.wall4.doors;
         // calcula a quantidade de portas

  const doorArea = door * (0.8 * 1.9);
    // calcula a area das portas

  const window = +wallsJson.wall1.windows
   + +wallsJson.wall2.windows
    + +wallsJson.wall3.windows
     + +wallsJson.wall4.windows;
            // calcula a quantidade de janelas

  const windowArea = window * (2 * 1.2);
    // calcula a area das janelas

  const total = totalArea - (doorArea - windowArea);
    // calcula a area total do quarto/sala sem as portas e janelas

    // verifica se a area total de todas as paredes individualmente é maior que 1 metro quadrado e menor que 50 metros quadrados
  if (wall1Area < 1 || wall1Area > 50) {
    return { error: true, message: '1. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.', parede1: wall1Area };
  }
  if (wall2Area < 1 || wall2Area > 50) {
    return { error: true, message: '2. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.', parede2: wall2Area };
  }
  if (wall3Area < 1 || wall3Area > 50) {
    return { error: true, message: '3. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.', parede3: wall3Area };
  }
  if (wall4Area < 1 || wall4Area > 50) {
    return { error: true, message: '4. Nenhuma parede pode ter menos de 1 metro quadrado nem mais de 50 metros quadrados, mas podem possuir alturas e larguras diferentes.', parede4: wall4Area };
  }

  // verifica se a area total do quarto/sala é maior que 50 metros quadrados
  if (doorArea + windowArea > (totalArea / 2)) {
    return {
      error: true, message: '2. O total de área das portas e janelas não pode ser maior que 50% da area da parede', doorArea, windowArea, totalArea,
    };
  }

  // verifica se a altura de paredes com porta é pelo menos 30 centimetros maior que a altura da porta
  if (wallsJson.wall1.doors > 0 && wallsJson.wall1.height < 2.20) {
    return { error: true, message: '3. A altura da parede não pode ser menor que 2.20 metros se ela possuir portas', parede1: wallsJson.wall1.height };
  }
  if (wallsJson.wall2.doors > 0 && wallsJson.wall2.height < 2.20) {
    return { error: true, message: '3. A altura da parede não pode ser menor que 2.20 metros se ela possuir portas', parede2: wallsJson.wall2.height };
  }
  if (wallsJson.wall3.doors > 0 && wallsJson.wall3.height < 2.20) {
    return { error: true, message: '3. A altura da parede não pode ser menor que 2.20 metros se ela possuir portas', parede3: wallsJson.wall3.height };
  }
  if (wallsJson.wall4.doors > 0 && wallsJson.wall4.height < 2.20) {
    return { error: true, message: '3. A altura da parede não pode ser menor que 2.20 metros se ela possuir portas', parede4: wallsJson.wall4.height };
  }

  // FUNÇÕES RECURSIVAS MAS PODEM SER ALTERADAS PARA MIDDLEWARES PARA IMPLEMENTAÇÕES DE OUTROS CALCULOS NO FUTURO (PISO, TETO, GESSO)

  const result = await paintWallModel.paintwallCalculator(total); // ENVIA A AREA TOTAL DO QUARTO/SALA PARA O MODEL
  console.log(result);
  return result;
};

module.exports = {
  wallCalculator,
};

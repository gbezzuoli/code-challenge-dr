
# DigitalRepublic Tintas (Calculadora de uso de tintas)

Projeto feito em NodeJS para o teste técnico da Digital Republic




## Documentação da API

#### Retorna todos os itens

```http
  post /wall
```

| Body   | Tipo       | Descrição                           |
| :---------- | :--------- | :---------------------------------- |
| `wall1height` | `number` | **Obrigatório**. A Altura da primeira parede|
| `wall1width` | `number` | **Obrigatório**. A Largura da primeira parede|
| `wall1doors` | `number` | **Obrigatório**. A quantidade de portas da primeira parede |
| `wall1window` | `number` | **Obrigatório**. A quantidade de janelas da primeira parede|
| `wall2height` | `number` | **Obrigatório**. A altura da segunda parede |
| `wall2width` | `number` | **Obrigatório**. A largura da segunda parede |
| `wall2doors` | `number` | **Obrigatório**. A quantidade de portas da segunda parede |
| `wall2window` | `number` | **Obrigatório**. A quantidade de janelas da segunda parede |
| `wall3height` | `number` | **Obrigatório**. A altura da terceira parede |
| `wall3width` | `number` | **Obrigatório**. A largura da terceira parede |
| `wall3doors` | `number` | **Obrigatório**. A quantidade de portas da terceira parede |
| `wall3window` | `number` | **Obrigatório**. A quantidade de janelas da terceira parede |
| `wall4height` | `number` | **Obrigatório**. A altura da quarta parede|
| `wall4width` | `number` | **Obrigatório**. A largura da quarta parede |
| `wall4doors` | `number` | **Obrigatório**. A quantidade de portas da quarta parede |
| `wall4window` | `number` | **Obrigatório**. A quantidade de janelas da quarta parede |


#### Retorna o numero de baldes e sua conveniencia (preenchendo do maior balde para o menor)

## Autores

- [@gbezzuoli](https://www.github.com/gbezzuoli)


## Instalação

Após a clonagem entra no diretorio do projeto (back-end)

```bash
  npm install 
  npm run start
```
    
## Rodando localmente


Instale as dependências

```bash
  npm install
```

Inicie o servidor

```bash
  npm run start
```
Abra com a interface grafica com liveserver dentro do VScode ou execute o arquivo index.html

```bash
  firefox index.html
```

## Stack utilizada

**Front-end:** HTML5, CSS

**Back-end:** Node, Express


const { bucketCount } = require('../utils/bucketCount');

const paintwallCalculator = async (total) => {
  const totalBuckets = await bucketCount(total); // calcula a quantidade de baldes de tinta
  // espaço para inserir no banco de dados, caso venha ser implementado
  return totalBuckets; // retorna a quantidade de baldes de tinta
};

module.exports = {
  paintwallCalculator,
};

const validateWalls = (req, res, next) => {
  const walls = req.body;
  if (!walls) {
    return res.status(400).send('No walls provided');
  }
  if (!walls.wall1height
     || !walls.wall2height
      || !walls.wall3height
       || !walls.wall4height) {
    return res.status(400).send('Invalid wall height');
  }
  if (!walls.wall1width
     || !walls.wall2width
      || !walls.wall3width
       || !walls.wall4width) {
    return res.status(400).send('Invalid wall width');
  }
  if (!walls.wall1doors
     || !walls.wall2doors
      || !walls.wall3doors
       || !walls.wall4doors) {
    return res.status(400).send('Invalid wall doors');
  }
  if (!walls.wall1window
     || !walls.wall2window
      || !walls.wall3window
       || !walls.wall4window) {
    return res.status(400).send('Invalid wall windows');
  }
  next();
};

const validateWallHeight = (req, res, next) => {
  const walls = req.body;
  if (walls.wall1height
     <= 0 || walls.wall2height
      <= 0 || walls.wall3height
       <= 0 || walls.wall4height <= 0) {
    return res.status(400).send('Invalid wall height');
  }
  next();
};

const validateDoor = (req, res, next) => {
  const walls = req.body;
  if (walls.wall1doors
     < 0 || walls.wall2doors
      < 0 || walls.wall3doors
       < 0 || walls.wall4doors
        < 0) {
    return res.status(400).send('Invalid door quantity');
  }
  next();
};

const validateWindow = (req, res, next) => {
  const walls = req.body;
  if (walls.wall1window
     < 0 || walls.wall2window
      < 0 || walls.wall3window
      < 0 || walls.wall4window
       < 0) {
    return res.status(400).send('Invalid window quantity');
  }
  next();
};

module.exports = {
  validateWalls,
  validateWallHeight,
  validateDoor,
  validateWindow,
};

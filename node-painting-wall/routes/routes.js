const express = require('express'); // Importa express
const bodyParser = require('body-parser'); // importa o body-parser, que é um middleware que faz o parse do body da requisição
const {
  validateWalls,
  validateWallHeight,
  validateDoor,
  validateWindow,
} = require('../middlewares/validateWalls'); // Importa os middlewares de validação
const { cleanData } = require('../utils/cleanData'); // Importa o middleware de limpeza de dados
const { wallCalculator } = require('../controllers/paintWallController'); // Importa o controller

const router = express.Router(); // Cria o router
router.use(bodyParser.json()); // Configura o body-parser
router.use(bodyParser.urlencoded({ extended: true })); // Configura o body-parser

router.post(
  '/',
  validateWalls,
  validateWallHeight,
  validateDoor,
  validateWindow,
  cleanData,
  wallCalculator,
); // Configura a rota /wall com os middlewares de validação e o controller

module.exports = router;

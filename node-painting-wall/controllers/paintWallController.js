const paintWallService = require('../services/paintWallService');
const { cleanData } = require('../utils/cleanData');
const HTTP_STATUS = require('http-status-codes');

const wallCalculator = async (req, res) => {
    const walls = req.body; // Pega o body da requisição
    const wallsJson = await cleanData(walls); // Limpa os dados do body
    const result = await paintWallService.wallCalculator(wallsJson); // Chama o service
    if (result.error) {
        return res.status(HTTP_STATUS.StatusCodes.BAD_REQUEST).json(result); // Retorna o erro
    }

    // este retorno pode ser alterado para enviar o resultado para um framework de front-end
    // este retorno tambem pode ser alterado para apenas enviar o resultado em json
    return res.status(HTTP_STATUS.StatusCodes.OK).send(`
  <html>
    <head>
        <title>Resultado</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
        body {
            font-family: 'Roboto', sans-serif;
            color: white;
            background-color: rgb(7, 7, 7);
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }

        .form-card
            {
            background-color: #F05A22;
            color: black;
            border-radius: 5px;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            padding: 40px;
            width: 60%;
            margin: 0 auto;
            margin-top: 40px;
            transition: 0.3s ease;
            }
        .form-grid
            {
            background-color: #F3901D;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            text-align: center;
            width: 60%;
            margin: 0 auto;
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2);
            margin: 10px;
            padding: 10px;
            border-radius: 12px;
            }
            </style>
    </head>
  <body>
        <div class="form-grid">
            <h1 class="form-card">Você terá que usar ${result.totalBuckets} baldes da sua tinta DigitalRepublic preferida:</h1>
            <p>${result.large18} Balde(s) 18l</p>
            <p>${result.medium3six} Balde(s) 3,6l</p>
            <p>${result.small2five} Balde(s) 2,5l</p>
            <p>${result.mini0five} Balde(s) 0,5l</p>
            <p>${result.total} metros quadrados usam ${result.totalLitros} Litros de tinta</p>
            <p>Irá sobrar ${result.remainder} litros de tinta</p>
        </div>
        <body>
        </html>`


    ); // Retorna o resultado
};

module.exports = {
    wallCalculator,
};
